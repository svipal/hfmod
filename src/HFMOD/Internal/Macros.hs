{-# LANGUAGE TemplateHaskell, QuasiQuotes, OverloadedStrings, ScopedTypeVariables #-}
module HFMOD.Internal.Macros where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.Map as Map
import qualified Data.ByteString as BS

import qualified Language.Haskell.TH as TH
import           Language.C.Inline.Context
import qualified Language.C.Types as C




-- nothing yet