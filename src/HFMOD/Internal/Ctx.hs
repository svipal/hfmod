{-# LANGUAGE TemplateHaskell, QuasiQuotes, OverloadedStrings, ScopedTypeVariables #-}
module HFMOD.Internal.Ctx where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.Map as Map
import qualified Data.ByteString as BS

import qualified Language.Haskell.TH as TH
import           Language.C.Inline.Context
import qualified Language.C.Types as C



data FmodStudioSystem
data FmodStudioBank
data FmodEventDescription
data FmodEventInstance
data FmodStudioAdvancedSettings
type FmodResult = CInt
type FmodStopMode = CInt
type FmodBool = CBool

fmodCtx :: Context
fmodCtx = C.baseCtx <> C.bsCtx <> ctx
  where
    ctx = mempty
      { ctxTypesTable = fmodTypesTable
      }

fmodTypesTable :: Map.Map C.TypeSpecifier TH.TypeQ
fmodTypesTable = Map.fromList
  [ (C.TypeName "FMOD_STUDIO_SYSTEM", [t| FmodStudioSystem |])
  , (C.TypeName "FMOD_RESULT", [t| FmodResult|])
  , (C.TypeName "FMOD_STUDIO_BANK", [t| FmodStudioBank |])
  , (C.TypeName "FMOD_STUDIO_EVENTDESCRIPTION", [t| FmodEventDescription |])
  , (C.TypeName "FMOD_STUDIO_EVENTINSTANCE", [t| FmodEventInstance |])
  , (C.TypeName "FMOD_STUDIO_ADVANCEDSETTINGS", [t| FmodStudioAdvancedSettings |])
  , (C.TypeName "FMOD_STUDIO_STOP_MODE", [t| FmodStopMode |])
  , (C.TypeName "FMOD_BOOL", [t| FmodBool |])
  ]


-- StopMode


