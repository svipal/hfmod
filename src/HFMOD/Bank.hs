{-# LANGUAGE TemplateHaskell, QuasiQuotes, OverloadedStrings, ScopedTypeVariables #-}
module HFMOD.Bank where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.ByteString as BS

import HFMOD.Internal.Ctx

C.context (fmodCtx)

C.include "<stdio.h>"
C.include "<FMOD/fmod.h>"
C.include "<FMOD/fmod_studio.h>"


loadBank  ::  Ptr FmodStudioSystem  -- FMOD System
          ->  Ptr FmodStudioBank    -- FMOD Bank
          ->  BS.ByteString         -- path to bank
          ->  IO FmodResult
loadBank fsystem fbank path = do
  let system = castPtr fsystem
  let bank = castPtr fbank
  r <- [C.block| int {
      FMOD_STUDIO_BANK* Bank = $(void* bank);
      
      FMOD_RESULT result = FMOD_Studio_System_LoadBankFile(
        $(void* system),
        $bs-ptr:path,
        FMOD_STUDIO_LOAD_BANK_NORMAL,
        &Bank
        );

      return result;
    }|]
  return  r