{-# LANGUAGE TemplateHaskell, QuasiQuotes, OverloadedStrings, ScopedTypeVariables #-}
module HFMOD.System where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.ByteString as BS

import HFMOD.Internal.Ctx

C.context (fmodCtx)

C.include "<stdio.h>"
C.include "<FMOD/fmod.h>"
C.include "<FMOD/fmod_studio.h>"



createFMODSystem :: IO (Ptr FmodStudioSystem)
createFMODSystem = do
  fmodPointer  <- [C.block| FMOD_STUDIO_SYSTEM* {
      static FMOD_STUDIO_SYSTEM* system = NULL;
      FMOD_RESULT result = FMOD_Studio_System_Create(&system, FMOD_VERSION);
      switch (result)
      {
          case FMOD_OK:
              printf("SYSTEM OK\n");
              break;
          
          default:
              printf("SYSTEM NOT OK LOL\n");
      }
      return system;
    }|]

  return $! fmodPointer



initFMODSystem  :: Ptr FmodStudioSystem 
                -> IO FmodResult
initFMODSystem system = do
  [C.block| int {
      FMOD_RESULT result = FMOD_Studio_System_Initialize(
        $(FMOD_STUDIO_SYSTEM* system),
        16,
        FMOD_STUDIO_INIT_NORMAL,
        FMOD_INIT_NORMAL,
        NULL);
      return result;
  }|]




updateSystem :: Ptr FmodStudioSystem
            -> IO FmodResult
updateSystem system = do
  [C.block| int {
      FMOD_Studio_System_Update(
      $(FMOD_STUDIO_SYSTEM* system));
    }|]




releaseSystem :: Ptr FmodStudioSystem
              -> IO FmodResult
releaseSystem system  = do
  [C.block| FMOD_RESULT { 
    FMOD_Studio_System_Release(
      $(FMOD_STUDIO_SYSTEM* system));
  }|]



setGlobalParameter  :: Ptr FmodStudioSystem
                    -> BS.ByteString
                    -> Float
                    -> Bool
                    -> IO FmodResult
setGlobalParameter system param  value ignoreseekspeed= do 
  let param' = BS.concat [param, "\0"]
  let cvalue = CFloat value
  [C.block| int { 
    FMOD_Studio_System_SetParameterByName(
      $(FMOD_STUDIO_SYSTEM* system),
      $bs-ptr:param',
      $(float cvalue),
      0); 
  }|]


setAdvancedSettings :: Ptr FmodStudioSystem
                    -> Ptr FmodStudioAdvancedSettings
                    -> IO FmodResult
setAdvancedSettings system settings = do
  [C.block| int {  
    FMOD_Studio_System_SetAdvancedSettings(
        $(FMOD_STUDIO_SYSTEM *system),
        $(FMOD_STUDIO_ADVANCEDSETTINGS *settings)
    );
  }|]