{-# LANGUAGE TemplateHaskell, QuasiQuotes, OverloadedStrings, ScopedTypeVariables #-}
module HFMOD.Lib 
  (module HFMOD.Bank
  ,module HFMOD.Event
  ,module HFMOD.EventInstance
  ,module HFMOD.System
  ,testFunc
  ,errcheck) where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.ByteString as BS

import HFMOD.Internal.Ctx

import HFMOD.Bank
import HFMOD.Event
import HFMOD.EventInstance
import HFMOD.System

C.context (fmodCtx)

C.include "<stdio.h>"
C.include "<FMOD/fmod.h>"
C.include "<FMOD/fmod_studio.h>"








testFunc = do
  s <- createFMODSystem
  initFMODSystem s  >>= errcheck "INIT : "

  vbank <- [C.block| void* {
    static FMOD_STUDIO_BANK* Bank; 
    return Bank;}|]
  let bank = castPtr vbank
  loadBank s bank "./Sound/Master.bank\0" >>= errcheck "BANK : "
  loadBank s bank  "./Sound/Master.strings.bank\0" >>= errcheck "STRINGBANK : "


  evDesc <- getEventDescription s "event:/Music\0"
  evInst <- getEventInstance evDesc

  startEventInstance evInst >>= errcheck "Instance Start"
  
  updateSystem s >>= errcheck "System update"

  threadDelay 2000000

  releaseSystem s >>= errcheck "System release"













errcheck what 0     = (print what) >>  print "all ok"
errcheck what 18    = (print what) >>  print "Error : File not found"
errcheck what 30    = (print what) >>  print "Error : Invalid Handle"
errcheck what 74    = (print what) >>  print "Error : Event not found"
errcheck what other = (print what) >>  (print $ "fail... error code : " ++ show other)

  
