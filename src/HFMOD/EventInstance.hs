{-# LANGUAGE TemplateHaskell, QuasiQuotes, OverloadedStrings, ScopedTypeVariables #-}
module HFMOD.EventInstance where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.ByteString as BS

import HFMOD.Internal.Ctx

C.context (fmodCtx)

C.include "<stdio.h>"
C.include "<FMOD/fmod.h>"
C.include "<FMOD/fmod_studio.h>"




getEventInstance :: Ptr FmodEventDescription
                -> IO (Ptr FmodEventInstance) 
getEventInstance event = do
  eventInstance <- [C.block| FMOD_STUDIO_EVENTINSTANCE* { 
      FMOD_STUDIO_EVENTINSTANCE* EventInstance;
      FMOD_RESULT result = FMOD_Studio_EventDescription_CreateInstance(
        $(FMOD_STUDIO_EVENTDESCRIPTION* event ),
        &EventInstance);
      switch (result)
        {
            case FMOD_OK:
                printf("EVENT INSTANCE OK\n");
                break;
            
            default:
                printf("EVENT INSTANCE NOT OK LOL\n");
                printf("Error code %d\n", result);
        }

      return EventInstance;
    }|]
  return $! castPtr eventInstance



startEventInstance :: Ptr FmodEventInstance
                  -> IO FmodResult
startEventInstance eInstance = do    
  r <- [C.block| int { 
      FMOD_Studio_EventInstance_Start(
      $(FMOD_STUDIO_EVENTINSTANCE* eInstance)); 
    }|]
  return  r


stopEventInstance :: Ptr FmodEventInstance
                  -> FmodStopMode
                  -> IO FmodResult
stopEventInstance eInstance mode = do    
  r <- [C.block| int { 
      FMOD_Studio_EventInstance_Stop(
      $(FMOD_STUDIO_EVENTINSTANCE* eInstance),
      $(FMOD_STUDIO_STOP_MODE mode)
      ); 
    }|]
  return  r





setLocalParameter  :: Ptr FmodEventInstance
                  -> BS.ByteString
                  -> Float
                  -> Bool
                  -> IO FmodResult
setLocalParameter eInstance param  value ignoreseekspeed= do
  let cvalue = CFloat value
  let param' = BS.concat [param, "\0"]
  let bignoreseekspeed = fromIntegral $ fromEnum ignoreseekspeed
  r <-[C.block| int { 
    FMOD_Studio_EventInstance_SetParameterByName(
      $(FMOD_STUDIO_EVENTINSTANCE* eInstance),
      $bs-ptr:param',
      $(float cvalue),
      $(int bignoreseekspeed)); }|]
  return  r


triggerCue  :: Ptr FmodEventInstance
            -> IO FmodResult
triggerCue eInstance = do
  [C.block| int {
    FMOD_Studio_EventInstance_TriggerCue($(FMOD_STUDIO_EVENTINSTANCE* eInstance));
  }|]


setReverbLevel  :: Ptr FmodEventInstance
                -> Int
                -> Float
                -> IO FmodResult
setReverbLevel eInstance range value  = do
  let crange = CInt $! fromIntegral range
  let cvalue = CFloat value
  [C.block| int { 
    FMOD_Studio_EventInstance_SetReverbLevel(
      $(FMOD_STUDIO_EVENTINSTANCE* eInstance),
      $(int crange),
      $(float cvalue));
  }|]


setPaused   :: Ptr FmodEventInstance
            -> Bool
            -> IO FmodResult
setPaused eInstance paused  = do
  let bpaused = fromIntegral $ fromEnum paused
  [C.block| int { 
    FMOD_Studio_EventInstance_SetPaused(
      $(FMOD_STUDIO_EVENTINSTANCE* eInstance),
      $(int bpaused));
  }|]



getPaused  :: Ptr FmodEventInstance
          -> IO (Maybe Bool)
getPaused eInstance = do
  let ispaused = nullPtr
  r <- [C.block| int { 
      FMOD_Studio_EventInstance_GetPaused(
        $(FMOD_STUDIO_EVENTINSTANCE* eInstance),
        $(int* ispaused));
    }|]
  case r of 
    0 -> do
      b <-  [C.block| int { return *$(int* ispaused); }|]
      return $! Just (toEnum $ fromIntegral b)
    _ -> do
      return Nothing

releaseEventInstance :: Ptr FmodEventInstance 
                     -> IO FmodResult
releaseEventInstance eInstance = do
  [C.block| int { 
    FMOD_Studio_EventInstance_Release($(FMOD_STUDIO_EVENTINSTANCE* eInstance));
  }|]

getTimelinePosition :: Ptr FmodEventInstance 
                    -> IO (Maybe Int)
getTimelinePosition eInstance = do 
  let position = nullPtr
  r <- [C.block| int { 
      FMOD_Studio_EventInstance_GetTimelinePosition(
        $(FMOD_STUDIO_EVENTINSTANCE *eInstance),
        $(int *position));
    }|]
  case r of 
    0 -> do
      b <- [C.block| int { return *$(int* position); }|]
      return $! Just (fromIntegral b)
    _ -> return Nothing