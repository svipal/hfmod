{-# LANGUAGE TemplateHaskell, QuasiQuotes, OverloadedStrings, ScopedTypeVariables #-}
module HFMOD.Event where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.ByteString as BS

import HFMOD.Internal.Ctx

C.context (fmodCtx)

C.include "<stdio.h>"
C.include "<FMOD/fmod.h>"
C.include "<FMOD/fmod_studio.h>"




getEventDescription :: Ptr FmodStudioSystem
                    -> BS.ByteString
                    -> IO (Ptr FmodEventDescription) 
getEventDescription system path = do
  evPointer <- [C.block| FMOD_STUDIO_EVENTDESCRIPTION* {
      FMOD_STUDIO_EVENTDESCRIPTION* EventMusic;
      FMOD_RESULT result = FMOD_Studio_System_GetEvent(
        $(FMOD_STUDIO_SYSTEM* system),
        $bs-ptr:path, 
        &EventMusic);
      switch (result)
      {
          case FMOD_OK:
              printf("Event OK\n");
              break;
          
          default:
              printf("Event NOT OK LOL\n");
              printf("Error code %d\n", result);
      }
      return EventMusic;
    }|]
  return $! castPtr evPointer