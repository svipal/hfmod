{-# LANGUAGE TemplateHaskell, QuasiQuotes, ScopedTypeVariables #-}
module Spec where
import qualified Language.C.Inline as C
import           Foreign.C.Types
import           Data.Monoid ((<>))
import Control.Monad
import Control.Concurrent
import Foreign.Ptr
import qualified Data.ByteString as BS

import Lib

main :: IO ()
main = putStrLn "Test suite not yet implemented"



testFunc = do
  -- Creation
  fmodPointer <- [C.block| long {
      static FMOD_STUDIO_SYSTEM* system = NULL;
      FMOD_RESULT result = FMOD_Studio_System_Create(&system, FMOD_VERSION);
      switch (result)
      {
          case FMOD_OK:
              printf("SYSTEM OK\n");
              break;
          
          default:
              printf("SYSTEM NOT OK LOL\n");
      }
      return (long)system;
    }|]

  -- Initialization

  [C.block| void {
      FMOD_STUDIO_SYSTEM* system = (FMOD_STUDIO_SYSTEM*)$(long fmodPointer);
      FMOD_RESULT result = FMOD_Studio_System_Initialize(
        system,
        16,
        FMOD_STUDIO_INIT_NORMAL,
        FMOD_INIT_NORMAL,
        NULL);
      switch (result)
      {
          case FMOD_OK:
              printf("SYSTEM INIT OK\n");
              break;
          
          default:
              printf("SYSTEM INIT NOT OK LOL\n");
      }
    }|]



  -- loading the banks

  r <- [C.block| int { 

      FMOD_STUDIO_BANK* SaviourBank;
      FMOD_STUDIO_SYSTEM* system = (FMOD_STUDIO_SYSTEM*)$(long fmodPointer);
      FMOD_RESULT result = FMOD_Studio_System_LoadBankFile(
          system,
          "./Sound/Master.bank",
          FMOD_STUDIO_LOAD_BANK_NORMAL,
          &SaviourBank
          );
      result =  FMOD_Studio_System_LoadBankFile(
          system,
          "./Sound/Master.strings.bank",
          FMOD_STUDIO_LOAD_BANK_NORMAL,
          &SaviourBank
          );
      return result;
    }|]


  -- loading the event description

  eventDescription <- [C.block| long { 
    FMOD_STUDIO_EVENTDESCRIPTION* SaviourMusic;
    FMOD_STUDIO_SYSTEM* system = (FMOD_STUDIO_SYSTEM*)$(long fmodPointer);
    FMOD_RESULT result = FMOD_Studio_System_GetEvent(system, "event:/Music", &SaviourMusic);

    switch (result)
    {
        case FMOD_OK:
            printf("MUSIC LOAD OK\n");
            break;
        
        default:
            printf("MUSIC LOAD NOT OK LOL\n");
    }
    return (long)SaviourMusic;
  }|]

  print eventDescription

  -- creating event Instance

  eventInstance <- [C.block| long { 
    FMOD_STUDIO_EVENTINSTANCE* SaviourInstance;
    FMOD_STUDIO_EVENTDESCRIPTION* SaviourMusic = (FMOD_STUDIO_EVENTDESCRIPTION*)$(long eventDescription);
    FMOD_RESULT result = FMOD_Studio_EventDescription_CreateInstance(
      SaviourMusic,
      &SaviourInstance);
    switch (result)
      {
          case FMOD_OK:
              printf("MUSIC INSTANCE OK\n");
              break;
          
          default:
              printf("MUSIC INSTANCE NOT OK LOL\n");
      }

    return (long)SaviourInstance;
  }|]

  print eventInstance

  -- starting music
  r <- [C.block| long {
    FMOD_STUDIO_EVENTINSTANCE* SaviourInstance = (FMOD_STUDIO_EVENTINSTANCE*)$(long eventInstance);
    FMOD_Studio_EventInstance_Start(SaviourInstance);
  }|]
  
  print r
  -- setting local parameter

  r <- [C.block| long { 
    FMOD_STUDIO_EVENTINSTANCE* SaviourInstance = (FMOD_STUDIO_EVENTINSTANCE*)$(long eventInstance);
    FMOD_Studio_EventInstance_SetParameterByName(SaviourInstance,"TURN",3,0);
  }|]

  print r

  -- updating the system
  r <- [C.block| long { 
    FMOD_STUDIO_SYSTEM* system = (FMOD_STUDIO_SYSTEM*)$(long fmodPointer);
    FMOD_Studio_System_Update(system);
    }|]

  print r
  threadDelay 5000000

  -- releasing the system
  r <- [C.block| long { 
    FMOD_STUDIO_SYSTEM* system = (FMOD_STUDIO_SYSTEM*)$(long fmodPointer);
    FMOD_Studio_System_Release(system);
    }|]
  print r
    